#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=CLang-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/CLang-Windows
CND_ARTIFACT_NAME_Debug=week2_uselab
CND_ARTIFACT_PATH_Debug=dist/Debug/CLang-Windows/week2_uselab
CND_PACKAGE_DIR_Debug=dist/Debug/CLang-Windows/package
CND_PACKAGE_NAME_Debug=week2uselab.tar
CND_PACKAGE_PATH_Debug=dist/Debug/CLang-Windows/package/week2uselab.tar
# Release configuration
CND_PLATFORM_Release=CLang-Windows
CND_ARTIFACT_DIR_Release=dist/Release/CLang-Windows
CND_ARTIFACT_NAME_Release=week2_uselab
CND_ARTIFACT_PATH_Release=dist/Release/CLang-Windows/week2_uselab
CND_PACKAGE_DIR_Release=dist/Release/CLang-Windows/package
CND_PACKAGE_NAME_Release=week2uselab.tar
CND_PACKAGE_PATH_Release=dist/Release/CLang-Windows/package/week2uselab.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
