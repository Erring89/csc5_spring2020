/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 29, 2020, 8:16 AM
 */

#include <cstdlib>
#include <iostream>
 // Using the C++ standard libraries
using namespace std; 

/*
 * 
 */
int main(int argc, char** argv) {
    //Get two numbers from the user
    int num1;
    int num2;
    
     //Get input from the user
    cout << "Please enter two numbers: ";
    cin >> num1 >> num2;
     // Calculate the average
    //Dont forget double vision 
    double avg = (num1 + num2) / 2.0;
    cout << "The average is: " << avg << endl;
    
    return 0;
}

