/** Name: Renee Talamantez
 *  Student ID: 2087697  
 *  Date: 03-21-20 
 *  HW: 3    
 * I certify this is my own work and code*/
 
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <ctime>




using namespace std;

/*
 * 
 */


int main(int argc, char** argv) 
{
            int menuChoice;
            cout << " Choose between 1 - 432: " << endl; 
            cout << "1. 10 whole numbers." << endl;
            cout << "2. BMR." << endl;
            cout << "3. exercises." << endl;
            cout << "4. Rock / Paper / Scissors." << endl;
            cin >> menuChoice;
            double lessZero = 0;
            double greaterZero = 0;
            double positiveNegative = 0;
            double numbers = 0;
            double negativeCounter = 0;
            double positiveCounter = 0;
            double everythingCounter = 0;
            double weight;
            double heightinInches;
            string gender;
            int age;
            double chocolateBars = 230;
            double bmr;
            string decision;
            double times = 0 ;
            double nums = 0;
            double totalPossible = 0;
            double scoreReceived = 0;
            double average = 0;
            double percentage;
            int playerOneChoice;
            int playerTwoChoice;
            string playerSelection;
            string yes;
            int rnd;
            srand(time(0));
            
           
          
            
            
            switch (menuChoice)
            { 
                case 1:
                    // <Random Formula> = rand() % 200 + (-100); 

                     cout << "Enter 10 whole numbers: " << endl;

                     for (int i = 0; i < 10; i++) //  i = i + 1 ;  i += 1;
                     {
                         //cout << "this is I :" << i << endl;
                         // cin >> numbers;
                         numbers = rand () % 200 + (-100);
                         cout << numbers << endl;
                         positiveNegative = positiveNegative + numbers;
                         everythingCounter = everythingCounter + 1;

                         if (numbers > 0)
                         {
                             greaterZero = greaterZero + numbers;
                             positiveCounter = positiveCounter + 1;
                         }

                         if (numbers < 0)
                         {
                             lessZero = lessZero + numbers ;
                             negativeCounter = negativeCounter + 1;

                         }
                     }


                    cout << setprecision(2) << fixed; 
                    cout << "Average of all Numbers: " <<  positiveNegative / everythingCounter * 1.0 <<endl; 
                    cout << "Average of Positive numbers: " << greaterZero / positiveCounter * 1.0 << endl;
                    cout << "Average of Negative numbers: " << lessZero / negativeCounter * 1.0 << endl; 
                    cout << "Total sum of all numbers: " << positiveNegative << endl;
                    cout << "Total sum Greater than zero: " << greaterZero << endl ; 
                    cout << "Total sum less than zero: " << lessZero << endl ; 
                    break;
                case 2: 
                    do 
                    {
                        cout << "Are you Male or Female? Enter M or F: " << endl;
                        cin >> gender;
                        cout << "What is your weight: " << endl;
                        cin >> weight;
                        cout << "How old are you?: " << endl;
                        cin >> age;
                        cout << "What is your height in inches: " << endl; 
                        cin >> heightinInches;

                        if (tolower(gender[0]) == 'm')
                        {
                            bmr = (66 + (6.3 * weight) + (12.9 * heightinInches) - (6.8 * age))/ chocolateBars;
                        }
                        else if (tolower(gender [0]) == 'f')
                        {
                          bmr = (655 + (4.3 * weight) +(4.7 * heightinInches) - (4.7 * age )) / chocolateBars; 
                        }
                        cout << "How many chocolateBars: " << endl;
                        cout << bmr << endl;

                        cout << "Do you want to run this again? (Y/N)" << endl;
                        cin >> decision;
                    }
                    while (tolower (decision[0]) == 'y');
                    break;
                case 3:
                    cout << "How many excersizes: " << endl;
                    cin >> times;

                    for (int i = 0; i < times; i++) 

                    {

                        cout << "Score received for exercise " << i + 1 << ": ";
                        cin >> nums ; 
                        scoreReceived = scoreReceived + nums ;
                        cout << "Total points possible: ";
                        cin >> nums;
                        totalPossible= totalPossible + nums ;
                        cout << endl;

                    }
                    //Your total is 22 out of 30, or 73.33%.
                    percentage = scoreReceived / totalPossible * 100.0;

                    cout << "Your total is " << scoreReceived 
                            << " out of " << totalPossible << ", or " << percentage << "%" << endl;
                    break;
                case 4:  
                    
                    
                    do
                    {
                        

                        playerOneChoice = rand() % 3; // 0=p  1=r  2=s
                         // show what player 1 picked
                        if (playerOneChoice == 0)
                        {
                            playerSelection = "Paper";
                        }
                        else if (playerOneChoice == 1)
                        {
                            playerSelection = "Rock";
                        }
                        else if (playerOneChoice == 2)
                        {
                            playerSelection = "scissors";
                        }
                        else
                        {
                            playerSelection = " WHAT HAPPENED? ";
                        }
                        cout << "Player 1 has selected " << playerSelection << endl;
                        //show what player 2 picked
                        playerTwoChoice = rand() % 3; // 0=p  1=r  2=s
                        // show what player 1 picked
                        if (playerTwoChoice == 0)
                        {
                            playerSelection = "Paper";
                        }
                        else if (playerTwoChoice == 1)
                        {
                            playerSelection = "Rock";
                        }
                        else if (playerTwoChoice == 2)
                        {
                            playerSelection = "scissors";
                        }
                        else
                        {
                            playerSelection = " WHAT HAPPENED? 2";
                        }
                        cout << "Player 2 has selected " << playerSelection << endl;
                        
                        switch (playerOneChoice)
                        {
                            case 1: //player 1 selected rock
                                switch (playerTwoChoice)
                                {
                                    case 0:
                                        cout << "Player two wins because paper smothers rock";
                                        break;
                                    case 1: 
                                        cout << "Tie" ;
                                        break;
                                    case 2:
                                        cout << "Rock wins scissors Player one wins" ;
                                        break;
                                    default:
                                        cout << "error1";                                }
                                break;
                            case 0 : //player 1 selected paper
                                switch (playerTwoChoice)
                                {
                                   case 0 :
                                        cout << "Paper against paper makes is a TIE!";
                                        break;
                                    case 1 : 
                                        cout << "Paper against rock makes player one Win!" ;
                                        break;
                                    case 2 :
                                        cout << "Scissors Cut Paper Player 2 WIns!" ;
                                        break;
                                    default: 
                                        cout << "error2";
                                }
                                break;
                            case 2 ://player 1 selected scissors
                                switch (playerTwoChoice)
                                {
                                   case 0 :
                                        cout << "Scissors Cuts paper Plater Two Wins!";
                                        break;
                                    case 1 : 
                                        cout << "Rock bangs scissors Player 2 Wins!";
                                        break;
                                    case 2 :
                                        cout << "Its a Tie!" ;
                                        break;
                                    default: 
                                        cout << "error3";
                                }
                                break;
                                
                            default:
                                cout << "error4";
                                //something went wrong        
                            
                        }
                        
                        
                        
                        cout << endl;
                        //ask user to play again y/N
                        cout << "play again?" <<endl;
                        cin >> yes;
                    }
                    while (tolower(yes[0])=='y') ;
                    
                   
                    
                    
                    
                    
                    break;
                default: 
                    cout << "Not an option" << endl;
            }                                

    return 0;
      
    }
//player 1 chose rock
// player 2 chose rock who won?
// player 2 chose paper who won?
// player 2 chose scissors who won?

//player 1 chose paper
// player 2 chose rock ??? who won?
// player 2 chose paper ??? who won?
// player 2 chose scissors ???who won?

//player 1 chose scissors
// player 2 chose rock ??? who won?
// player 2 chose paper ??? who won?
// player 2 chose scissors ??? who won?