/**  Name:Renee Talamantez 
Student ID:2087697*  
Date:3/7/20* 
HW:homework1*  
Problem:* 
I certify this is my own work and code*/

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //Problem 1 
    int numb1 = 50;
    int numb2 = 100;
    
    cout << "Sum of both numbers is: " ;
    double sum = (numb1 + numb2) ; 
    cout << sum << endl;
    
    
    
    // Problem 2
    double totalsalestax = 95.00 ;
    double statetax = .04 ; 
    double countytax = .02 ;
    
    
    double total1 =  totalsalestax*statetax ;
    double total2 =  totalsalestax*countytax ;
   
    cout << "What is the total for the state tax: " << total1 << endl;
    cout << "What is the total for the county tax:  " << total2 << endl;
     //Problem 3       
    double tax;
    double mealcost; 
    double tip;
    
    mealcost = 88.67;
    tax = .0675 * mealcost;
    tip = .20 * mealcost; 
    
    cout << "What is the total tip with taxes included"; 
    
    double total = tax + mealcost + tip;
    
    cout << "Tip Amount: " << tip << endl; 
    cout << "Meal cost: " << mealcost << endl;
    cout << "Tax amount: " << tax << endl;
    cout << "Total: " << total << endl ;
    
    //Problem 4 
    double gallons = 15;
    double holds = 375;
    
    double totally = holds / gallons ; 
    
    cout << "Number of miles per gallon the car gets: " << totally << endl;
    
    
    // Problem 5
    cout << "Renee" << endl
    << "9515558888" << endl
    << "Computer Programming" << endl ; 

    return 0;
}

