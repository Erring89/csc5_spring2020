/**Name: Renee Talamantez
 * Student ID: 2087697 
 * Date: 04/2/20  
 * HW: 5 
 * Problem: 1 , 4 , 5 
 * I certify this is my own work and code*/


#include <cstdlib>
#include <iostream>
#include <iomanip>


using namespace std;

void userRockPaper();
void AIRockPaper();

int main(int argc, char** argv) {
    /**
     * selects the option of game
     * @param argc
     * @param argv
     * @return 
     */
    int input;
    cout << "Select A program:" << endl;
    cout << "1) rock paper scissors game" << endl;
    cout << "2) AI rock paper scissors " << endl;
    cin >> input;

    switch (input) {
        case 1:
            userRockPaper();
            break;
        case 2:
            AIRockPaper();
            break;
        default:
            cout << "Goodbye";
    }

    return 0;
}

 
void userRockPaper() {
    int playerOneChoice;
    int playerTwoChoice;
    string playerSelection;
    string yes;
    int rnd;
    srand(time(0));
    bool done = false;
 
   
  
   
    cout << "user rock paper player chooses" << endl;

    
    
    
    do {

        playerOneChoice = rand() % 3; // 0=p  1=r  2=s
        // show what player 1 picked
        if (playerOneChoice == 0) {
            playerSelection = "Paper";
        } else if (playerOneChoice == 1) {
            playerSelection = "Rock";
        } else if (playerOneChoice == 2) {
            playerSelection = "scissors";
        } else {
            playerSelection = " WHAT HAPPENED? ";
        }
        cout << "Player 1 has selected " << playerSelection << endl;
        //show what player 2 picked
        playerTwoChoice = rand() % 3; // 0=p  1=r  2=s
        // show what player 1 picked
        if (playerTwoChoice == 0)
 {
            playerSelection = "Paper";
            done = true;
        } else if (playerTwoChoice == 1) {
            playerSelection = "Rock";
            done = true;
        } else if (playerTwoChoice == 2) {
            playerSelection = "scissors";
            done = true;
        } else {
            playerSelection = " WHAT HAPPENED? 2";
            done = true;
        }
        cout << "Player 2 has selected " << playerSelection << endl;
    } while (!done);

}

void AIRockPaper()

{
    int playerOneChoice = 1 + rand() % 2;
    int playerTwoChoice;
    string yes;
    cout << "User input AI" << endl;
    do {
        switch (playerOneChoice)
 {
            case 1: //player 1 selected rock
                switch (playerTwoChoice) {
                    case 0:
                        cout << "Player two wins because paper smothers rock";
                        break;
                    case 1:
                        cout << "Tie";
                        break;
                    case 2:
                        cout << "Rock wins scissors Player one wins";
                        break;
                    default:
                        cout << "error1";
                }
                break;
            case 0: //player 1 selected paper
                switch (playerTwoChoice) {
                    case 0:
                        cout << "Paper against paper makes is a TIE!";
                        break;
                    case 1:
                        cout << "Paper against rock makes player one Win!";
                        break;
                    case 2:
                        cout << "Scissors Cut Paper Player 2 WIns!";
                        break;
                    default:
                        cout << "error2";
                }
                break;
            case 2://player 1 selected scissors
                switch (playerTwoChoice) {
                    case 0:
                        cout << "Scissors Cuts paper Plater Two Wins!";
                        break;
                    case 1:
                        cout << "Rock bangs scissors Player 2 Wins!";
                        break;
                    case 2:
                        cout << "Its a Tie!";
                        break;
                    default:
                        cout << "error3";
                }
                break;

            default:
                cout << "error4";
                //something went wrong        

        }

        cout << endl;
        cout << "play again?" << endl;
        cin >> yes;
    } while (yes == "y");






    cout << "Not an option" << endl;
}
