/**Name: Renee Talamantez
 * Student ID: 2087697 
 * Date: 04/2/20  
 * HW: 5
 * Problem: 6
 * I certify this is my own work and code*/

#include <cstdlib>
#include <iostream>

using namespace std;

double poundsAndOunces(string);
double calculateWeight (double pounds , double ounces);
double calculateWeightTwo(double pounds , double kilos);
double calculateWeightThree(double grams);
void displayCalculations( double pounds, double grams,double kilograms ,double ounces);


int main(int argc, char** argv) 



{
   
      double pounds;
      double ounces;
      double ouncesToPounds;
      double poundsCovertedKilos;
      double kilosCovertedtoGrams;
      char runAgain = 'y';
/**
 * displaying pounds and ounces input 
 * stating functions converstions
 * displaying if want to run again and input 
 * @param argc
 * @param argv
 * @return 
 */
        do
      {
       
           pounds = poundsAndOunces("What are the Pounds "); 
           ounces = poundsAndOunces("What are the Ounces ");
           ouncesToPounds = calculateWeight(pounds , ounces );
           poundsCovertedKilos = calculateWeightTwo(pounds , ounces);
           kilosCovertedtoGrams= calculateWeightThree(poundsCovertedKilos);
           displayCalculations(pounds,kilosCovertedtoGrams,poundsCovertedKilos,ounces);
    cout<< " Do you want to run again" << endl;
    cin >> runAgain;
        } while (runAgain!='n');
    return 0;
    
}
/**
 * calculating ounces to pounds 
 * @param pounds
 * @param ounces
 * @return 
 */
double calculateWeight (double pounds , double ounces)
{
    double ouncesToPounds = ounces / 15 ;
    return ouncesToPounds;
   
}
/**
 * calculating pounds to kilos
 * @param pounds
 * @param ounces
 * @return 
 */
double calculateWeightTwo(double pounds , double ounces)
{
    double poundsToKilos = pounds / 2.2046;  
    return poundsToKilos;
}
/**
 * calculating kilos to grams
 * @param grams
 * @return 
 */
double calculateWeightThree(double grams)
{
    double kilosToGrams = grams / 1000;
    return kilosToGrams;
}
void displayCalculations(double pounds,double grams,double kilograms,double ounces)
/**
 * displaying converstions
 * @param display
 * @return 
 */
{
      // Your X POUNDS and Y OUNCES converted into  A kilograms and B grams
    cout << "The pounds and ounces converted into grams is " << endl;
    cout << "ounces    : " << ounces   << endl;
    cout << "pounds    : " << pounds   << endl;
    cout << "grams     : " << grams    << endl;
    cout << "kilograms : " << kilograms<< endl; 
}
/**
 * Displaying input and returning it 
 * @param display
 * @return 
 */
double poundsAndOunces(string display) // end user entered value
{
    cout << display;
    double input;
    cin >> input;        
    return input;
}
