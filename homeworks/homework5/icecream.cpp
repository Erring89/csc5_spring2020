/**  Name:Renee Talamantez
 * Student ID: 2087697   Date: 04/2/20
 *  HW: 5 Problem: 3 
 *  I certify this is my own work and code*/
#include <cstdlib>
#include <iostream>

using namespace std;

int getCustomerAmount();
double getIceCreamWeight();
double calculatePortionSize(int customers , double iceCreamWeight);
int main(int argc, char** argv) 
{
    /**
     * calculates the amount per portion size of customer
     * @param argc
     * @param argv
     * @return 
     */
    int amountOfCustomers;
    double iceCreamWeight;
    double portionSize;
    
    amountOfCustomers = getCustomerAmount();
    iceCreamWeight = getIceCreamWeight();
    portionSize =  calculatePortionSize( amountOfCustomers, iceCreamWeight);
    
    cout << "The amount per customers portion size is " << portionSize << "lbs" << endl; 
    return 0;
}
/**
 * gets amount of customers
 * @return 
 */
int getCustomerAmount() 
{   
    int amountOfCustomers;
    cout << "Enter Amount of Customers" << endl;
    cin >> amountOfCustomers;
    return amountOfCustomers;
}
  /**
 * gets the user input of the weight of the ice cream
 * @return 
 */
double getIceCreamWeight()


{
    double iceCreamWeight;
    cout << "Enter the weight of ice cream" << endl;
    cin >> iceCreamWeight;
    return iceCreamWeight;
}
/**
 * caluculating the amount of ice cream customer gets
 * @param customers
 * @param iceCreamWeight
 * @return 
 */
double calculatePortionSize(int customers , double iceCreamWeight)
{
 double portionSize  =  iceCreamWeight / customers;
 return portionSize;
}


