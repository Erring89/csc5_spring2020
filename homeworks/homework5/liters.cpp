/**Name: Renee Talamantez
 * Student ID: 2087697 
 * Date: 04/2/20  
 * HW: 5 
 * Problem: 2
 * I certify this is my own work and code*/

#include <cstdlib>
#include <iostream>
using namespace std;

const double litersTogasoline = 0.264179; 

double calculateMPG(double userMilesone, double gasolineConsumeone);

int main(int argc, char** argv) {
    
 
    double userLitersone= 0;
    double milesTraveledone =0;
    double userMilesone = 0;
    double gasolineConsumeone= 0;
    char runAgain = 'y';
   
    
         
        
    
    do 
       {
          cout  << "Input liters of gasoline consumed  " << endl;
          cin   >>  userLitersone;
         
          gasolineConsumeone = userLitersone * litersTogasoline;
        
        cout << " Input total miles driven from "  << endl;
        cin  >> userMilesone;
        
        
        milesTraveledone = calculateMPG(userLitersone, userMilesone);
       
        cout << "Miles Traveled Per Gallon: " << milesTraveledone << endl;
           
        cout << "Do you want to calculate again" << endl;
        cin  >> runAgain;
        
      } while (runAgain!='n');
    
    

    
    

    return 0;
}

double calculateMPG(double userMilesone, double gasolineConsumeone)
{
   double milesTraveledone = userMilesone / gasolineConsumeone;
   return milesTraveledone;
}