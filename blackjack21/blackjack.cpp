#include <iostream>
#include <ctime>
#include <cstdlib>
#include <time.h>
using namespace std;
void blackJack(double &dollars);
void slots(double &dollars);
int getRandomSym();
char numToSym(int num);
int getCard(int);


int main()
{ 
    int menuChoice;
    double dollars = 100.00;
    char runAgain = 'y';    
    srand(time(NULL));
    
   
    while (runAgain != 'n' || dollars <= 0)
    {
        cout << "Welcome let's lose money!" << endl;
        cout << "1 for blackjack" << endl;
        cout << "2 for slots" << endl;
        cin >> menuChoice;        
        
        switch (menuChoice)
        {  
            case 1:
                blackJack(dollars);
                break;
            case 2: 
                slots(dollars);
                break;
            default :
                cout << "not an option" << endl;
                break;
        }
        
        cout << "Current Amount: $" << dollars << endl;
 
        cout << "Play again?" << endl;
        cin >> runAgain;        
    }
    
    if(dollars <= 0)
    {
        cout << "No more money to play with!" << endl;
    }
    return 0;
 }
 
    

int getCard(int total) {
    int addCard, playerNewTotal;
    addCard = rand() % 10 + 1;
    playerNewTotal = total + addCard;

    return playerNewTotal;
}
void blackJack(double &dollars)
{
    srand(time(0));
    int totalPlayer, totalHouse, playerCard1, playerCard2, playerNewTotal, houseCard1, houseCard2, houseNewTotal = 0;
    string choice, playAgain = "y";
    double bet;
    bool playerBust = false;
    bool houseBust = false;

    cout << "BLACK JACK GAME " << endl;
    cout << "Enter Dollar amount for bet $1-100: $ ";
    cin  >> bet;
    playerCard1 = getCard(0);
    playerCard2 = getCard(0);
    houseCard1 = getCard(0);
    houseCard2 = getCard(0);
    totalPlayer = playerCard1 + playerCard2;
    totalHouse = houseCard1 + houseCard2;

    cout << "Player first two cards: " << playerCard1 << ", " << playerCard2 << endl;
    cout << "House first two cards:" << houseCard1 << " , " << houseCard2 << endl;
    cout << "Player  total is " << totalPlayer << "." << endl;
    cout << "House total is " << totalHouse << " , " << endl;

    cout << "Would you like to  Hit ? (y/n)" << endl;
    cin >> choice;
    choice = tolower(choice[0]);


    while (choice == "y") 
    {
        if (totalPlayer == 21) {

            cout << "BlackJack!!!" << endl;
            cout << " You win $ :" << bet *2 << endl;
            dollars += bet *2;
            cout << "Your new winnings are $ : " << endl;
        }
        else if (totalPlayer < 21) 
        {
            totalPlayer = getCard(totalPlayer);
            cout << "Player hit your new total: " << totalPlayer << endl;
           

            if (totalPlayer > 21) {
                cout << "You bust!" << endl;
                choice = 'n';
                playerBust = true;

            } else {
                cout << "Would you like to hit again? y= hit n=STAY" << endl;
                cin >> choice;
            }
        }
        else
        {
        }
    }

    int softTotal = 0;
    int hardTotal = totalHouse;

    if (houseCard1 == 1) 
    {
        softTotal = 11;
    } else 
    {
        softTotal += houseCard1;
    }
    if (houseCard2 == 1) 
    {
        softTotal += 11;
    } 
    else 
    {
        softTotal += houseCard2;
    }

    if (softTotal < 17) 
    {
        totalHouse = getCard(softTotal);
    } 
    else if (hardTotal < 17) 
    {
        totalHouse = getCard(hardTotal);
    }
    else
    {
    }

    while (totalHouse < 17 && playerBust != true) 
    {       

        if (totalHouse < 17) 
        {
            totalHouse = getCard(totalHouse);
        }

        cout << "The houses new total: " << totalHouse << endl;
        if (totalHouse > 21) 
        {                
            cout << "The House busted: " << totalHouse << endl;
            houseBust = true;
        } 
        else if (totalHouse == 21) 
        {                 
            cout << "BlackJack!!!!" << endl;
        } 
        else 
        {

        }
    }

    if (playerBust == true) {
        dollars -= bet;
        cout << "Players bet is lost" << endl;
        cout << "Dollars left: " << dollars << endl;

        cout << "House wins because player Bust" << endl;
        cout << "The house total: " << totalHouse << endl;
        cout << "The players total: " << totalPlayer << endl;
    } 
    else if (totalPlayer == totalHouse) 
    {
        cout << " Its a Tie   " << totalHouse << totalPlayer << endl;
    } 
    else if (totalPlayer > totalHouse || houseBust == true) {
        dollars += bet *2; 
        cout << "Player wins   " << endl;
        cout << "The amount you win is $: " << bet *2 << endl;
        cout << "The new dollars amount you have is $: " << dollars << endl;
        cout << "Players total: " << totalPlayer << endl;
        cout << "Houses Total: " << totalHouse << endl;

    } 
    else if (totalHouse > totalPlayer && houseBust != true) 
    {
        dollars -= bet;
        cout << "Players bet is lost" << endl;
        cout << "Dollars left: " << dollars << endl;
        cout << "House Wins" << endl;
        cout << "The house total: " << totalHouse << endl;
        cout << "The player Total: " << totalPlayer << endl;
    } 
    else 
    {
        cout << "Error" << endl;

    }     
}

void slots(double &dollars)
{      
    bool firstRow = false;
    bool secondRow = false;
    bool thirdRow = false;
    double bet;
 
    int p1 = 0;
    int p2 = 0;
    int p3 = 0;
    int p4 = 0;
    int p5 = 0;
    int p6 = 0;
    int p7 = 0;
    int p8 = 0;
    int p9 = 0;
 
    char s1 = ' ';
    char s2 = ' ';
    char s3 = ' ';
    char s4 = ' ';
    char s5 = ' ';
    char s6 = ' ';
    char s7 = ' ';
    char s8 = ' ';
    char s9 = ' ';
 
    p1 = getRandomSym();
    p2 = getRandomSym();
    p3 = getRandomSym();
    p4 = getRandomSym();
    p5 = getRandomSym();
    p6 = getRandomSym();
    p7 = getRandomSym();
    p8 = getRandomSym();
    p9 = getRandomSym();
 
    
 
    s1 = numToSym(p1);
    s2 = numToSym(p2);
    s3 = numToSym(p3);
    s4 = numToSym(p4);
    s5 = numToSym(p5);
    s6 = numToSym(p6);
    s7 = numToSym(p7);
    s8 = numToSym(p8);
    s9 = numToSym(p9);
 
 
    cout << "Welcome to Rainy Day Slots!" << endl;
    cout << "How much would you like to bet:" << endl;
    cin >> bet;
 
    if (bet > dollars)
    {
        cout << "You don't got the money for it!" << endl;
    }
    else
    {
        cout << s1 << " " << s2 << " " << s3 << endl;
        cout << s4 << " " << s5 << " " << s6 << endl;
        cout << s7 << " " << s8 << " " << s9 << endl;
        cout << "--------------------------" << endl;
 
        if ((s1 == s2) && (s1 == s3))
        {
            cout << "First row all match!" << endl;
            dollars += bet * 2;
            firstRow = true;
        }
        else if ((s2 == s1) && (s2 == s3))
        {
            cout << "First row all match!" << endl;
            dollars += bet * 2;
            firstRow = true;
        }
        else if ((s3 == s1) && (s3 == s2))
        {
            cout << "First row all match!" << endl;
            dollars += bet * 2;
            firstRow = true;
        }
        else if ((s1 == s2) && (s3 == 'X'))
        {
            cout << "First row all match!" << endl;
            dollars += bet * 2;
            firstRow = true;
        }
        else if ((s1 == s3) && (s2 == 'X'))
        {
            cout << "First row all match!" << endl;
            dollars += bet * 2;
            firstRow = true;
        }
        else if ((s2 == s1) && (s3 == 'X'))
        {
            cout << "First row all match!" << endl;
            dollars += bet * 2;
            firstRow = true;
        }
        else if ((s2 == s3) && (s1 == 'X'))
        {
            cout << "First row all match!" << endl;
            dollars += bet * 2;
            firstRow = true;
        }
        else if ((s3 == s1) && (s2 == 'X'))
        {
            cout << "First row all match!" << endl;
            dollars += bet * 2;
            firstRow = true;
        }
        else if ((s3 == s2) && (s1 == 'X'))
        {
            cout << "First row all match!" << endl;
            dollars += bet * 2;
            firstRow = true;
        }
        else if ((s2 == 'X') && (s3 == 'X'))
        {
            cout << "First row all match!" << endl;
            dollars += bet * 2;
            firstRow = true;
        }
        else if ((s1 == 'X') && (s3 == 'X'))
        {
            cout << "First row all match!" << endl;
            dollars += bet * 2;
            firstRow = true;
        }
        else if ((s1 == 'X') && (s2 == 'X'))
        {
            cout << "First row all match!" << endl;
            dollars += bet * 2;
            firstRow = true;
        }
        else if ((s1 == 'X') && (s2 == 'X') && (s3 == 'X'))
        {
            cout << "First row all match!" << endl;
            dollars += bet * 2;
            firstRow = true;
        }
        else
        {
            
        }
 
        if ((s4 == s5) && (s4 == s6))
        {
            cout << "Second row all match!" << endl;
            dollars += bet * 2;
            secondRow = true;
        }
        else if ((s5 == s4) && (s5 == s6))
        {
            cout << "Second row all match!" << endl;
            dollars += bet * 2;
            secondRow = true;
        }
        else if ((s6 == s4) && (s6 == s5))
        {
            cout << "Second row all match!" << endl;
            dollars += bet * 2;
            secondRow = true;
        }
        else if ((s4 == s5) && (s6 == 'X'))
        {
            cout << "Second row all match!" << endl;
            dollars += bet * 2;
            secondRow = true;
        }
        else if ((s4 == s6) && (s5 == 'X'))
        {
            cout << "Second row all match!" << endl;
            dollars += bet * 2;
            secondRow = true;
        }
        else if ((s5 == s4) && (s6 == 'X'))
        {
            cout << "Second row all match!" << endl;
            dollars += bet * 2;
            secondRow = true;
        }
        else if ((s5 == s6) && (s4 == 'X'))
        {
            cout << "Second row all match!" << endl;
            dollars += bet * 2;
            secondRow = true;
        }
        else if ((s6 == s4) && (s5 == 'X'))
        {
            cout << "Second row all match!" << endl;
            dollars += bet * 2;
            secondRow = true;
        }
        else if ((s6 == s5) && (s4 == 'X'))
        {
            cout << "Second row all match!" << endl;
            dollars += bet * 2;
            secondRow = true;
        }
        else if ((s5 == 'X') && (s6 == 'X'))
        {
            cout << "Second row all match!" << endl;
            dollars += bet * 2;
            secondRow = true;
        }
        else if ((s4 == 'X') && (s6 == 'X'))
        {
            cout << "Second row all match!" << endl;
            dollars += bet * 2;
            secondRow = true;
        }
        else if ((s4 == 'X') && (s5 == 'X'))
        {
            cout << "Second row all match!" << endl;
            dollars += bet * 2;
            secondRow = true;
        }
        else if ((s4 == 'X') && (s5 == 'X') && (s6 == 'X'))
        {
            cout << "Second row all match!" << endl;
            dollars += bet * 2;
            secondRow = true;
        }
        else
        {
            
        }
 
        if ((s7 == s8) && (s7 == s9))
        {
            cout << "Third row all match!" << endl;
            dollars += bet * 2;
            thirdRow = true;
        }
        else if ((s8 == s7) && (s8 == s9))
        {
            cout << "Third row all match!" << endl;
            dollars += bet * 2;
            thirdRow = true;
        }
        else if ((s9 == s7) && (s9 == s8))
        {
            cout << "Third row all match!" << endl;
            dollars += bet * 2;
            thirdRow = true;
        }
        else if ((s7 == s8) && (s9 == 'X'))
        {
            cout << "Third row all match!" << endl;
            dollars += bet * 2;
            thirdRow = true;
        }
        else if ((s7 == s9) && (s8 == 'X'))
        {
            cout << "Third row all match!" << endl;
            dollars += bet * 2;
            thirdRow = true;
        }
        else if ((s8 == s7) && (s9 == 'X'))
        {
            cout << "Third row all match!" << endl;
            dollars += bet * 2;
            thirdRow = true;
        }
        else if ((s8 == s9) && (s7 == 'X'))
        {
            cout << "Third row all match!" << endl;
            dollars += bet * 2;
            thirdRow = true;
        }
        else if ((s9 == s7) && (s8 == 'X'))
        {
            cout << "Third row all match!" << endl;
            dollars += bet * 2;
            thirdRow = true;
        }
        else if ((s9 == s8) && (s7 == 'X'))
        {
            cout << "Third row all match!" << endl;
            dollars += bet * 2;
            thirdRow = true;
        }
        else if ((s8 == 'X') && (s9 == 'X'))
        {
            cout << "Third row all match!" << endl;
            dollars += bet * 2;
            thirdRow = true;
        }
        else if ((s7 == 'X') && (s9 == 'X'))
        {
            cout << "Third row all match!" << endl;
            dollars += bet * 2;
            thirdRow = true;
        }
        else if ((s7 == 'X') && (s8 == 'X'))
        {
            cout << "Third row all match!" << endl;
            dollars += bet * 2;
            thirdRow = true;
        }
        else if ((s7 == 'X') && (s8 == 'X') && (s9 == 'X'))
        {
            cout << "Third row all match!" << endl;
            dollars += bet * 2;
            thirdRow = true;
        }
        else
        {
            
        }
 
        if (firstRow == false && secondRow == false && thirdRow == false)
        {
            dollars -= bet;
        }
        else
        {
            
        }
    }
}
    
int getRandomSym()
{
    int sym = rand() % 6 + 1;
 
    return sym;
}
 
char numToSym(int num)
{
    char sym = ' ';
    switch (num)
    {
    case 1:
        sym = 'A';
        break;
    case 2:
        sym = 'B';
        break;
    case 3:
        sym = 'C';
        break;
    case 4:
        sym = 'D';
        break;
    case 5:
        sym = 'E';
        break;
    case 6:
        sym = 'X';
        break;
    }
 
    return sym;
}
